

# Usage
    $ cd ftp
    $ docker build -t ftp .
    $ docker run -d -v "/home/lvnar/Projects/ftp/data:/home/vsftpd" \
                -p 20:20 \
                -p 21:21 \
                -p 20000-21000:20000-21000 \
                -e FTP_USER=lvnar \
                -e FTP_PASS=password \
                -e PASV_ADDRESS=192.168.0.113 \
                --name ftp \
				--restart=always ftp
